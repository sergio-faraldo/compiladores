#ifndef LEX_YY_H
#define LEX_YY_H

#include "elem.h"
/*
 * En este analizador léxico se ha implementado el sistema de enviado de DEDENT e INDENT en lugar de simplemente enviar
 * tabuladores.
 * Para el archivo wilcoxon.h 4 espacios == 1 tab, con lo cual se usa la siguiente configuración:
 * #define caracterEspacio ' '
 * #define numEspaciosTab 4
 *
 * En caso de que el número de espacios sea distinto se puede modificar para adaptarse a otros ficheros.
 * En caso de que el fichero use tabuladores, se debería usar la siguiente configuración:
 * #define caracterEspacio '\t'
 * #define numEspaciosTab 1
 * */
void cambiarYyin(char path[]);
void inicializarLexico();
void cerrarLexico();
#endif //LEX_YY_H
