#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include "TablaSimbolos.h"
#include "sintactico.tab.h"
#include "Comandos.h"

HashTable *tablaSimbolos;
double *pi;
double *e;

/*
 * Estas funciones servirán para encapsular las de la tabla hash y así hacer el código más legible y fácil de programar
 */

int estaEnTSimbolos(char clave[]) {
    return hashtable_contains_key(tablaSimbolos, clave);
}
int anhadirElemATSimbolos(char clave[], int valor, void* mem) {//realizo copias de la clave para que se añada bien a la tabla
    char *p = malloc(sizeof(char) * (strlen(clave) + 1));
    strcpy(p, clave);
    elem *e=malloc(sizeof(elem));
    *e=crearElem(clave, valor, mem);
    return hashtable_add(tablaSimbolos, p, e);
}
elem getValTSimbolos(char clave[]) {
    elem *value;
    hashtable_get(tablaSimbolos, clave, (void *) &value);
    return *value;
}
double getValNumTSimbolos(char clave[]){
    elem *value;
    hashtable_get(tablaSimbolos, clave, (void *) &value);
    return *value->puntDouble;
}
int actualizarValorVar(char clave[], double valor){
    elem *value;
    printf("%s||%f\n",clave,valor);
    if (hashtable_contains_key(tablaSimbolos, clave)) {
        hashtable_get(tablaSimbolos, clave, (void *) &value);
        *(value->puntDouble)=valor;
        return CC_OK;
    }
    return CC_ERR_KEY_NOT_FOUND;
}
void destruirTSimbolos() {
    hashtable_destroy(tablaSimbolos);
    free(e);
    free(pi);
}

void imprimirTSimbolos() {//esta función aprovecha el iterador incluido en hashtable.h
    HashTableIter hti;
    TableEntry *entry;

    hashtable_iter_init(&hti, tablaSimbolos);

    while (hashtable_iter_next(&hti, &entry) != CC_ITER_END) {
        imprimirElem(*(elem *) ((*entry).value));
    }//(*entry)  porque defino entry como un puntero. Dentro de eso basta con .key y .value para acceder a los campos.
}    //*(int *) ... Porque el valor se guarda en un puntero, con el  *  accedo a su valor.

void inicializarTSimbolos() {
    if (hashtable_new(&tablaSimbolos) != CC_OK) {//creo la tabla hash con los valores por defecto
        printf("Error reservando memoria para la tabla de símbolos\n");
        exit(EXIT_FAILURE);
    }
    anhadirElemATSimbolos("sin", FUNCT, &sin);
    anhadirElemATSimbolos("cos", FUNCT, &cos);
    anhadirElemATSimbolos("tan", FUNCT, &tan);
    anhadirElemATSimbolos("asin", FUNCT, &asin);
    anhadirElemATSimbolos("acos", FUNCT, &acos);
    anhadirElemATSimbolos("atan", FUNCT, &atan);
    anhadirElemATSimbolos("exp", FUNCT, &exp);
    anhadirElemATSimbolos("log", FUNCT, &log10);
    anhadirElemATSimbolos("ln", FUNCT, &log);
    anhadirElemATSimbolos("sqrt", FUNCT, &sqrt);
    pi=malloc(sizeof(double));
    *pi=M_PI;
    anhadirElemATSimbolos("PI", CONST, pi);
    e=malloc(sizeof(double));
    *e=M_E;
    anhadirElemATSimbolos("E", CONST, e);
    anhadirElemATSimbolos("print", COMM, &imprimirTSimbolos);
    anhadirElemATSimbolos("help", COMM, &help);
    anhadirElemATSimbolos("clear", COMM, &clear);
    anhadirElemATSimbolos("clean", COMM, &clean);
    anhadirElemATSimbolos("load", COMM1PAR, &load);
    anhadirElemATSimbolos("quit", COMM, &quit);
}