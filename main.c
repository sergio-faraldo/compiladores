#include <stdio.h>
#include <unistd.h>
#include "lex.yy.h"
#include "TablaSimbolos.h"
#include "sintactico.tab.h"

int main(int argc, char *argv[]) {
    inicializarLexico();
    printf("Para ayuda, escribe help. Para salir, escribe quit\n>\t");
    yyparse();
    cerrarLexico();
}