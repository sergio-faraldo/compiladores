%option noyywrap yylineno
%{
#include <string.h>
#include "TablaSimbolos.h"
#include "Errores.h"
#include "lex.yy.h"
#include "sintactico.tab.h"

void lanzarErrorLexico(int c){
    lanzarError(c, yytext, yylineno);
}
/*
 * Esta función sirve para ayudar con la unión implícita de líneas.
 * No comprueba que todas las expresiones con (,[ o { se cierren, sino que se asegura de que la primera
 * se ha cerrado.
 * */
int numParentAbiertos = 0, charParentAb, charParentCerr;
void aux_parentesis(char c) {
    if (c == '(' || c == '[' || c == '{') {//compruebo que me manda algo que se tiene que cerrar
        if (numParentAbiertos == 0) {//si es el primer paréntesis que llega, guardo los datos correspondientes
            charParentAb = c;//( || [ || {
            numParentAbiertos++;
            if (c == '(')charParentCerr = ')';
            else if (c=='|')charParentCerr='|';
            else charParentCerr = charParentAb + 2;//con esto me ahorro un if, ya que '['=91 y ']'=93, y '{'=
        } else if (c == charParentAb)numParentAbiertos++;//si no es el primero, compruebo sea del mismo tipo y aumento el núm de abiertos
    } else {
        if (numParentAbiertos > 0 && c == charParentCerr)numParentAbiertos--;//si es uno de cierre, compruebo que coincida y resto al num de abiertos
    }
}

int aux_alfanum(){
  if(estaEnTSimbolos(yytext)){
      elem g=getValTSimbolos(yytext);//intento obtener el componente léxico a partir del lexema
      if(g.componente==VAR||g.componente==CONST){
              yylval.puntDouble=g.puntDouble;
          } else {
              yylval.puntFunc=g.puntFunc;
          }
      return g.componente;//si lo encuentro devuelvo el componente léxico
  }else{
      double *mem=malloc(sizeof(int));
      *mem=0;
      anhadirElemATSimbolos(yytext,VAR,(void*)mem);//si no lo encuentro, es una variable nueva
      yylval.puntDouble=mem;
      return VAR;
  }
}
%}
LOWERCASE [a-z]
UPPERCASE [A-Z]
ESCAPESEQ \\.
SSTRINGCHAR [^\\\n"']
LETTER  {LOWERCASE}|{UPPERCASE}
ALFANUM  ({LETTER}|_)({LETTER}|[0-9]|_)*
DECIMAL [1-9][0-9]*|0
OCTAL (0[oO][0-7]+)|0[0-7]*
HEX 0[xX][0-9a-fA-F]+
BIN 0[bB](0|1)+
INT {DECIMAL}|{OCTAL}|{HEX}|{BIN}
FPUNTO ([0-9]*\.[0-9]+)|[0-9]\.
FEXPONENTE ([0-9]+|{FPUNTO})[eE][+-]?[0-9]+
%%
[ \t]*
\n {
    if(numParentAbiertos==0)return '\n';
    printf(">\t");
    return yylex();
}
(\"({SSTRINGCHAR}|{ESCAPESEQ}|\')*\")|(\'({SSTRINGCHAR}|{ESCAPESEQ}|\")*\') {
    yylval.String = strdup(yytext);
    return STRING;
}
{INT}|{FPUNTO}|{FEXPONENTE} {
    yylval.valDouble=atof(yytext);
    return DOUBLE;
}
{ALFANUM} return aux_alfanum();
[*\^+-/%;=] return (int)yytext[0];
[()|] {
    aux_parentesis(yytext[0]);
    return (int)yytext[0];
}
<<EOF>> {
          fclose(yyin);
          yypop_buffer_state();
          /* Make sure we stop if the EOF is the original input. */
          if (!YY_CURRENT_BUFFER) { yyterminate(); }
          return '\n';
          }
. {
    lanzarErrorLexico(SIM_DESC);
    printf(">\t");
    return yylex();
}
%%

void cambiarYyin(char path[]){
    char *p=path;
    p++;
    p[strlen(p)-1]='\0';
    FILE* file = fopen(p, "r");
    if (file) {
         yyin = file;
         yypush_buffer_state(yy_create_buffer(yyin, YY_BUF_SIZE));
    } else {
         printf("Failed to open file: %s\n", p);
    }
}

void inicializarLexico(){
    yyin = stdin;
    inicializarTSimbolos();
}

void cerrarLexico(){
    destruirTSimbolos();
}

void yyerror (char const *s)
{
  printf ("%s\n", s);
}