CC= gcc
MAIN= practica3
SRCS = *.c
DEPS = *.h
$(MAIN): $(SRCS) $(DEPS)
	bison -d sintactico.y
	lex lexico.l
	$(CC) -o $(MAIN) $(SRCS) -lm
