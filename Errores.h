#ifndef COMPILADORES_ERRORES_H
#define COMPILADORES_ERRORES_H

#define TAM_EXC -2 //-1 es EOF, no quiero cunfundirlos, así que empiezo desde -2
#define SIM_DESC -3
#define SIM_INESP -4
#define EOF_INESP -5
#define ERR_INDENT -6
#define ERR_DIV_CERO -7

void lanzarError(int err, char lexem[], int lineno);

#endif //COMPILADORES_ERRORES_H
