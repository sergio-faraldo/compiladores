#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "elem.h"
#include "sintactico.tab.h"

/*
 * Esta función realiza copias de lexema y componente, las guarda en la estructura par y devuelve el resultado
 */
elem crearElem(char *lexema, int componente, void *mem) {
    elem p;
    p.lexema=strndup(lexema, TAM_ARRAYS);
    p.componente=componente;
    if(componente==VAR||componente==CONST){
        p.puntDouble=mem;
    } else {
        p.puntFunc=mem;
    }
    return p;
}

void imprimirElemDB(elem p){
    printf("%s\t-> %d\n",p.lexema,p.componente);
}
void imprimirElem(elem p){
    if(p.componente==VAR)
    printf("%s\t-> %f\n",p.lexema,*p.puntDouble);
}