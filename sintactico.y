%{
#include <math.h>
#include <stdio.h>
#include "Errores.h"
extern int yylex();
extern void yyerror (char const *s);
%}
%error-verbose
%union{
    char* String;
    double valDouble;
    double *puntDouble;
    double (*puntFunc)();
}
%type <valDouble> expression
%token <String> STRING
%token <valDouble> DOUBLE
%token <puntDouble> VAR CONST
%token <puntFunc> FUNCT COMM COMM1PAR

%right '='
%left '+' '-'
%left '*' '/' '%'
%right '^'
%%

input:
	| input line
	| error '\n'{ yyerrok;}
;
line: '\n'
	| expression '\n' { printf(">\t"); }
	| expression ';' '\n' { printf("----%f\n>\t",$1); }
	| COMM '\n' { (*$1)(); printf(">\t"); }
	| COMM1PAR STRING '\n' { (*$1)($2); printf(">\t"); }
;
expression: DOUBLE
		| VAR  '=' expression   {
                              *$1=$3;
                              $$=$3;
                            }
    | VAR  { $$=*$1; }
    | CONST  { $$=*$1; }
    | CONST '=' expression { printf("No se puede modificar una constante\n"); }
    | expression '+' expression { $$ = $1 + $3; }
    | expression '-' expression { $$ = $1 - $3; }
    | expression '*' expression { $$ = $1 * $3; }
    | expression '/' expression { if($3==0) {lanzarError(ERR_DIV_CERO, "", 0);$$=0;} else $$ = $1 / $3;}
    | expression '%' expression { if($3==0) {lanzarError(ERR_DIV_CERO, "", 0);$$=0;} else $$ = (int)$1 % (int)$3;}
    | expression '^' expression { $$ = pow($1,$3); }
    | FUNCT '(' expression ')'   {
                                  $$=(*$1)($3);
                                }
    | '|' expression '|' {
                                    if($2 > 0)
                                      $$ = $2;
                                    else
                                      $$ = -$2;
                           }
    | '-' expression        {$$ = -$2;}
    | '(' expression ')'    { $$ = $2; }
 ;
