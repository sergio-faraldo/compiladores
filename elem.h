#ifndef COMPILADORES_ELEM_H
#define COMPILADORES_ELEM_H
#define TAM_ARRAYS 32
/*
 * Esta estructura se utilizará para devolver componentes léxicos al analizador sintáctico.
 * A pesar de que bastaría con enviar el int componente, añado el lexema por motivos de gestión
 *  de errores y para depurar el código fácilmente.
 */

typedef struct{
    int componente;
    char *lexema;
    union{
        double *puntDouble;
        double (*puntFunc)();
    };
}elem;

elem crearElem(char *lexema, int componente, void *mem);

void imprimirElemDB(elem p);

void imprimirElem(elem p);

#endif //COMPILADORES_ELEM_H
