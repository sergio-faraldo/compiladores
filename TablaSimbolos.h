#ifndef COMPILADORES_TABLASIMBOLOS_H
#define COMPILADORES_TABLASIMBOLOS_H
#include "elem.h"
#include "hashtable.h"
void inicializarTSimbolos();
int estaEnTSimbolos(char clave[]);
elem getValTSimbolos(char clave[]);
int anhadirElemATSimbolos(char clave[], int valor, void* mem);
int actualizarValorVar(char clave[], double valor);
double getValNumTSimbolos(char clave[]);
void destruirTSimbolos();
void imprimirTSimbolos();

#endif //COMPILADORES_TABLASIMBOLOS_H
