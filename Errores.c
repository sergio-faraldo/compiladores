#include <stdio.h>
#include "Errores.h"

void lanzarError(int err, char lexem[], int lineno){
    printf("\n\n!!!!!!!!!!!!!!!!\nError encontrado:\n");
    switch (err){
        case TAM_EXC:
            printf(" Tamaño máximo de lexema excedido\n");
            break;
        case SIM_INESP:
            printf(" Caracter inesperado\n");
            break;
        case SIM_DESC:
            printf(" Caracter desconocido\n");
            break;
        case ERR_INDENT:
            printf(" Error de indentación\n");
            break;
        case ERR_DIV_CERO:
            printf(" Division entre cero\n!!!!!!!!!!!!!!!!\n\n");
            return;
    }
    printf("Extracto:\n----------------\n%s\n----------------\nLinea: %d\n!!!!!!!!!!!!!!!!\n\n", lexem, lineno);
}
