#include <stdio.h>
#include "Comandos.h"
#include "TablaSimbolos.h"
#include "lex.yy.h"
#include "sintactico.tab.h"

double help(){
    printf("---Funciones matemáticas disponibles:\n"
           "Sintaxis funcion(argumento)\n"
           "sin\n"
           "cos\n"
           "tan\n"
           "asin - Arco seno\n"
           "acos - Arco coseno\n"
           "atan - Arco tangente\n"
           "exp - e elevado a \n"
           "log - logaritmo base 10\n"
           "ln\n"
           "sqrt\n"
           "---Constantes predefinidas:\n"
           "PI\n"
           "E\n"
           "---Comandos:\n"
           "help\n"
           "print - Imprime todas las variables y su valor\n"
           "clear - Limpia la pantalla\n"
           "clean - Elimina todas las variables\n"
           "load \"path/al/archivo\" - Ejecuta el contenido de un archivo\n"
           "quit - Sale del programa");
    return 1;
}
double clear(){
    system("@cls||clear");
    return 1;
}
double clean(){
    destruirTSimbolos();
    inicializarTSimbolos();
    return 1;
}
double load(char* path){
    cambiarYyin(path);
    return 1;
}
double quit(){
    exit(EXIT_SUCCESS);
}
